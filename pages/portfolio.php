<div id="portfolio-section" class="row portfolio px-3 py-3 my-4 mb-5">
    <div class="col-12 text-center">
        <h2>Portfolio</h2>
    </div>
    <div class="col-lg-6 col-md-12 col-sm-12 my-3 website-wrapper">
        <a href="https://catch-up-learning.herokuapp.com/" target="_blank"><img src="./../assets/images/CatchUp.png"
                alt="" class="site-image"></a>
    </div>
    <div class="col-lg-6 col-md-12 col-sm-12 my-3 portfolio-items">
        <h5>Catch Up Learning</h5>
        <p class="text-muted">MongoDB, Express, React, Node JS</p>
        <p>A booking system for tutorial sessions with login validation, error-handling, sorting, and
            auto-update features. Hosted on Heroku and MongoDB Atlas. With full CRUD of tutors, users and
            transactions. Tutors can readily edit their account, set their available schedules and
            timeslots.
            Users can book tutors only on the unbooked timeslot and date. Users can also sort tutors
            depending on availability, subject handled and verification status. Includes admin features. <a
                href="https://catch-up-learning.herokuapp.com/" class="view-site" target="_blank"><strong>View site
                    >></strong></a></p>

    </div>
    <div class="col-lg-6 col-md-12 col-sm-12 my-3 website-wrapper">
        <a href="http://mayfair-rentals.herokuapp.com/" target="_blank">
            <img src="./../assets/images/Mayfair.png" alt="" class="site-image"></a>
    </div>
    <div class="col-lg-6 col-md-12 col-sm-12 my-3">
        <h5>Mayfair Rentals</h5>
        <p class="text-muted">Laravel, MySQL</p>
        <p>An asset management system for a formal events rental business with login validation, sorting
            features (by type, origin and label) and user. Hosted on Heroku and remotemysql, used AWS S3 for
            image handling. <a href="http://mayfair-rentals.herokuapp.com/" class="view-site"
                target="_blank"><strong>View site
                    >></strong></a></p>

    </div>
    <div class="col-lg-6 col-md-12 col-sm-12 my-3 website-wrapper">
        <a href="https://elite-talents-modelling-agency.herokuapp.com/" target="_blank"><img
                src="./../assets/images/Elite.png" alt="" class="site-image"></a>
    </div>
    <div class="col-lg-6 col-md-12 col-sm-12 my-3">
        <h5>Elite Talents Modelling Agency</h5>
        <p class="text-muted">MongoDB, Express, React, Node JS</p>
        <p>Audition website for aspiring models made with react. Includes banner, registration/login and multiple image
            upload features. <a href="https://elite-talents-modelling-agency.herokuapp.com/" class="view-site"
                target="_blank"><strong>View
                    site >></strong></a></p>

    </div>
    <div class="col-lg-6 col-md-12 col-sm-12 my-3 website-wrapper">
        <a href="https://rcferrer.gitlab.io/socionics-society/" target="_blank"><img
                src="./../assets/images/Socionics.png" alt="" class="site-image"></a>
    </div>
    <div class="col-lg-6 col-md-12 col-sm-12 my-3">
        <h5>Socionics Society</h5>
        <p class="text-muted">HTML, CSS, Sass, Bootstrap</p>
        <p>A static website about a personality theory with modals and form. Hosted on Gitlab. <a
                href="https://rcferrer.gitlab.io/socionics-society/" class="view-site" target="_blank"><strong>View site
                    >></strong></a>
        </p>


    </div>
    <div class="col-lg-6 col-md-12 col-sm-12 my-3 website-wrapper">
        <div class="overlay"></div>
        <a href="https://odin-on-rails.gitlab.io/wikipedia-homepage/" target="_blank"><img
                src="./../assets/images/Wiki.png" alt="" class="site-image"></a>
    </div>
    <div class="col-lg-6 col-md-12 col-sm-12 my-3 mb-5">
        <h5>Wikipedia Homepage</h5>
        <p class="text-muted">HTML, CSS, Sass, Bootstrap</p>
        <p>Mockup of a typical Wikipedia Homepage. <a href="https://odin-on-rails.gitlab.io/wikipedia-homepage/"
                class="view-site" target="_blank"><strong>View site >></strong></a></p>
    </div>

</div>