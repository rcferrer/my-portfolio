<div id="about-me-section" class="row about-me px-3">
    <div class="col-12 pt-3 text-center">
        <h2>What I Do</h2>
    </div>
</div>
<div class="about-me-cards row pb-5">
    <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="row d-flex service-card">
            <div class="my-auto mx-auto col-lg-3 col-md-3 col-sm-12">
                <img src="./../assets/images/service1.png" alt="" class="service-image">
            </div>
            <div class="my-auto mx-auto col-lg-9 col-md-9 col-sm-12 service-text">
                <h6 class="skill-title">Frontend development</h6>
                <p>I can make websites of any kind that are responsive using HTML, CSS, Javascript, Bootstrap, React
                    and
                    Sass. I can create the frontend of the website from scratch or copy from design with accuracy.
                </p>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="row d-flex service-card">
            <div class="my-auto mx-auto col-lg-3 col-md-3 col-sm-12">
                <img src="./../assets/images/service2.png" alt="" class="service-image">
            </div>
            <div class="my-auto mx-auto col-lg-9 col-md-9 col-sm-12 service-text">
                <h6 class="skill-title">Backend development</h6>
                <p>I can create and manage the backend of your website using Laravel/PHP and NodeJS. I have
                    knowledge of integrating both SQL/noSQL databases in websites and can host on AWS as well. </p>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="row d-flex service-card">
            <div class="my-auto mx-auto col-lg-3 col-md-3 col-sm-12">
                <img src="./../assets/images/service3.png" alt="" class="service-image">
            </div>
            <div class="my-auto mx-auto col-lg-9 col-md-9 col-sm-12 service-text">
                <h6 class="skill-title">Web Design</h6>
                <p>I can create mobile and desktop designs for your website with good UI/UX interface, as well as
                    improve the design of your website.</p>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="row d-flex service-card">
            <div class="my-auto mx-auto col-lg-3 col-md-3 col-sm-12">
                <img src="./../assets/images/service4.png" alt="" class="service-image">
            </div>
            <div class="my-auto mx-auto col-lg-9 col-md-9 col-sm-12 service-text">
                <h6 class="skill-title">Graphic Design/Illustration</h6>
                <p>I sometimes do illustrations as well. I can create logos, templates and creative works of almost
                    any art style. </p>
            </div>
        </div>
    </div>

    <div class="col-12 d-flex justify-content-center mt-3 mb-4">
        <a href="https://www.linkedin.com/in/rozelyn-claire-ferrer/" class="cta view-resume" target="_blank">VIEW
            LINKEDIN</a>
    </div>

</div>