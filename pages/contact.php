<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div id="contact-section" class="row contact px-3 text-center">
        <div class="col-12">
            <h2 class="mb-2">Contact Me</h2>
            <h5 class="mb-4 ">What do you want to create? Let's make that happen.</h5>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12 px-0 mx-auto">

            <div class="px-3">
                <form id="myForm" class="mx-0 px-0">
                    <div class="form-group">
                        <input type="text" placeholder="Full name" id="name" />
                    </div>
                    <div class="form-group">
                        <input type="email" placeholder="Email address" id="email" />
                    </div>
                    <div class="form-group">
                        <input type="text" placeholder="Subject" id="subject" />
                    </div>
                    <div class="form-group">
                        <textarea id="body" id="" placeholder="Message here" rows="10"></textarea>
                    </div>
                    <button class="button-submit">SEND
                        MESSAGE</button>
                </form>
            </div>
        </div>
    </div>